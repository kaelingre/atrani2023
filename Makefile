LATEX=pdflatex
LATEXOPT=--shell-escape
NONSTOP=--interaction=nonstopmode

LATEXMK=latexmk
LATEXMKOPT=-pdf

SECTIONS=sections/01.tex sections/02.tex sections/03.tex sections/04.tex sections/05.tex sections/06.tex sections/07.tex

all: main.pdf

main.pdf: main.tex preamble.tex $(SECTIONS)
	$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" main

clean:
	rm -f *~ *.log *.aux *.pdf figures/*.pdf *.auxlock *.fls *.toc *.old *.out *.fdb_latexmk

.PHONY: all
