\begin{Exc}
  Compute the color-ordered 2-scalar-2-gluon tree level amplitudes in massive scalar QCD using BCFW.
  \begin{Sol}
    The only Feynman vertex rule we are going to need is
    \begin{equation}
      \begin{tikzpicture}
        [
          baseline={([yshift=-0.5ex]current bounding box.center)}
        ]
        \draw[line] (0,0) node[left] {$1^s$} -- node[below,near start] {$\overset{\leftarrow}{p_1}$} node[below,near end] {$\overset{\rightarrow}{p_4}$} (2,0) node[right] {$4^s$};
        \draw[gluon] (1,1) node[above] {$2,\mu$} -- node[right] {$\uparrow p_2$} (1,0);
      \end{tikzpicture}
      = -\frac{i g}{\sqrt{2}}(p_1^\mu-p_4^\mu)
    \end{equation}
    It is convenient to use the spinor-helicity formalism.
    Hence, we use the expressions for the gluon polarization vectors:
    \begin{equation}
      \begin{aligned}
        \varepsilon_2^{+\mu}&=-\frac{1}{\sqrt{2}}\frac{[2|\gamma^\mu|r\rangle}{\langle 2r\rangle}\,,\\
        \varepsilon_2^{-\mu}&=\frac{1}{\sqrt{2}}\frac{\langle2|\gamma^\mu|r]}{[ 2r]}\,.
      \end{aligned}
    \end{equation}
    We have thus the two three-point amplitudes for the two different gluon-polarizations
    \begin{equation}
      \begin{aligned}
        \cA(1^s2^+4^s) &= i g\frac{[2|1|r\rangle}{\langle 2r\rangle}\,,\\
        \cA(1^s2^-4^s) &= i g\frac{\langle2|1|r]}{[ 2r]}\,.
      \end{aligned}
    \end{equation}
    Let us start with the distinct-helicity 4-pt amplitude $\cA(1^s2^+3^-4^s)$.
    We do only possible BCFW shift using the spinor-helicity formalism, i.e. we shift the massless gluons:
    \begin{equation}
      \begin{aligned}
        |\hat{2}\rangle &= |2\rangle-z|3\rangle\,, &  |\hat{2}] &= |2]\,,\\
        |\hat{3}\rangle &= |3\rangle\,, &  |\hat{3}] &= |3]+z|2]\,.
      \end{aligned}
    \end{equation}
    There is only one possible diagram in this BCFW shift
    \begin{equation}
      \begin{aligned}
        \cA(1^s2^+3^-4^s) &=
        \begin{tikzpicture}
          [
            baseline={([yshift=-0.5ex]current bounding box.center)}
          ]
          \node[draw,circle,inner sep=5pt] at (-0.8,0) (L) {L};
          \node[draw,circle,inner sep=5pt] at (0.8,0) (R) {R};
          \draw[line] (L.east) -- node[below] {$\overset{\rightarrow}{p_5}$} node[above] {$\hat{5}^s$} (R.west);
          \draw[line] (-1.6,-0.9) node[left] {$1^s$} -- (L.south west);
          \draw[gluon] (-1.6,0.9) node[left] {$\hat{2}^+$} -- (L.north west);
          \draw[gluon] (1.6,0.9) node[right] {$\hat{3}^-$} -- (R.north east);
          \draw[line] (1.6,-0.9) node[right] {$4^s$} -- (R.south east);
        \end{tikzpicture}\\
        &= - \hat{\cA}_\mathrm{L}\frac{i}{p_5^2-m^2}\hat{\cA}_\mathrm{R}\\
        &= \frac{-i}{\tau_{12}}g^2 \frac{[\hat{2}|1|r_1\rangle}{\langle\hat{2}r_1\rangle}\frac{\langle\hat{3}|4|r_2]}{[\hat{3}r_2]}\,,
      \end{aligned}
    \end{equation}
    where we used $p_5^2-m^2=(p_1+p_2)^2-m^2=2p_1\cdot p_2\equiv \tau_{12}$ and momentum conservation at each vertex to clean up the expression.
    A clever choice of reference spinors is $r_1=3$ and $r_2=2$, leading to
    \begin{equation}
      \begin{aligned}
        \cA(1^s2^+3^-4^s) &= \frac{-i}{\tau_{12}}g^2\frac{[\hat{2}|1|3\rangle\langle\hat{3}|4|2]}{\langle\hat{2}3\rangle[\hat{3}2]}\\
        &=\frac{-i}{\tau_{12}}g^2\frac{[2|1|3\rangle\langle3|4|2]}{s_{23}}\\
        &=\frac{i}{\tau_{12}}g^2\frac{[2|1|3\rangle^2}{s_{23}}\,,
      \end{aligned}
    \end{equation}
    where the shift rules and momentum conservation was used.

    The other amplitude we want to compute is the $++$ helicity version $\cA(1^s2^+3^+4^s)$.
    We do the same shift and chose the reference spinors $r_1=3$ and $r_2=\hat{2}$
    \begin{equation}
      \begin{aligned}
        \cA(1^s2^+3^+4^s) &= \frac{ig^2}{\tau_{12}} \frac{[\hat{2}|1|3\rangle}{\langle\hat{2}3\rangle}\frac{[\hat{3}|4|\hat{2}\rangle}{\langle3\hat{2}\rangle}\\
         &= \frac{ig^2}{\tau_{12}} \frac{[\hat{2}|1|3\rangle[\hat{3}|1|\hat{2}\rangle}{\langle23\rangle^2}\\
         &= \frac{ig^2}{\tau_{12}} \frac{\tr_+(\hat{2}1\hat{3}1)}{\langle23\rangle^2}\,.
      \end{aligned}
    \end{equation}
    Now, it is useful to remember that the location of the pole in $z$ is determined by $\tau_{1\hat{2}}=2p_1\cdot p_{\hat{2}}=0$.
    Using the commutation rules of the gamma matrices and this equation we find
    \begin{equation}
      \slashed{\hat{p}}_2\slashed{p}_1 = - \slashed{p}_1\slashed{\hat{p}}_2+\underbrace{2p_1\cdot \hat{p}_2}_{=0}\,,
    \end{equation}
    and, thus,
    \begin{equation}
      \tr_+(\hat{2}1\hat{3}1)=\tr_+(1\hat{2}\hat{3}1)=\tr_-(11\hat{2}\hat{3})=p_1^2\tr_-(\hat{2}\hat{3})=m^2\langle\hat{2}\hat{3}\rangle[\hat{3}\hat{2}]\,.
    \end{equation}
    Putting it together we finally find
    \begin{equation}
      \cA(1^s2^+3^+4^s) = \frac{ig^2m^2[23]}{\tau_{12}\langle23\rangle}\,.
    \end{equation}
    All other helicity amplitudes can be obtained from these two by flipping bra and ket.
  \end{Sol}
\end{Exc}
