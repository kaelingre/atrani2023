\begin{Exc}[Linearized Einstein equations for non-trivial background.]
  The goal is to derive the linearized Einstein equations about a non-trivial (generic) background
  $g_{\mu\nu}=g^{(0)}_{\mu\nu}+h_{\mu\nu}^{(1)}+\cO(\epsilon^2)$.
  \paragraph{Step 1:} Show for any vector field~$w$ 
  \begin{equation}
    (\nabla_\beta-\nabla_\beta^{(0)})w^\alpha = C^\alpha_{\beta\gamma} w^\gamma
  \end{equation}
  with
  \begin{equation}
    C^\alpha_{\beta\gamma} =\frac{1}{2}g^{\alpha\mu}_{(0)}\left(\nabla_\beta^{(0)}h_{\gamma\mu}^{(1)}+\nabla_\gamma^{(0)}h_{\beta\mu}^{(1)}-\nabla_\mu^{(0)}h_{\beta\gamma}^{(1)}\right)+\cO(\epsilon^2)
  \end{equation}
  \begin{Sol}
    From your GR course you should remember the definition of the covariant derivative and the Christoffel symbols
    \begin{equation}\label{eq:covVector}
      \nabla_\alpha w^\beta = \partial_\alpha w^\beta + {\Gamma^\beta}_{\alpha\gamma} w^\gamma\,,
    \end{equation}
    such that we immediately have
    \begin{equation}
      C^\alpha_{\beta\gamma} = {\Gamma^\alpha}_{\beta\gamma}-{\Gamma^{(0)\alpha}}_{\beta\gamma}\,.
    \end{equation}
    The Christoffel symbols can be expressed in terms of the metric (see your GR course again)
    \begin{equation}
      {\Gamma^\alpha}_{\beta\gamma}=\frac{1}{2}g^{\alpha\mu}\left(\partial_\beta g_{\gamma\mu}+\partial_\gamma g_{\beta\mu}-\partial_\mu g_{\beta\gamma}\right)
    \end{equation}
    Plugging this expression in and expanding in $\epsilon$ we arrive at
    \begin{equation}\label{eq:Cabc}
      C^\alpha_{\beta\gamma}=\frac{1}{2}g^{\alpha\mu}_{(0)}\left(\partial_\beta h_{\gamma\mu}^{(1)}+\partial_\gamma h_{\beta\mu}^{(1)}-\partial_\mu h_{\beta\gamma}^{(1)}\right)-\frac{1}{2}h^{\alpha\mu}_{(0)}\left(\partial_\beta g_{\gamma\mu}^{(0)}+\partial_\gamma g_{\beta\mu}^{(0)}-\partial_\mu g_{\beta\gamma}^{(0)}\right)+\cO(\epsilon^2)\,,
    \end{equation}
    where the relative sign is due to the inverse of the metric, see Exercise~\ref{exc:1}.
    Note that the terms at order $\epsilon^0$ have cancelled.
    We finally use the rule for acting with a covariant derivative on a symmetric $(0,2)$-tensor, which should also be familiar from your GR course:
    \begin{equation}\label{eq:covTensor}
      \nabla_\beta t_{\gamma\mu} = \partial_\beta t_{\gamma\mu}-{\Gamma^\alpha}_{\beta\gamma}t_{\alpha\mu}-{\Gamma^\alpha}_{\beta\mu}t_{\alpha\gamma}\,.
    \end{equation}
    Solving this equation for the term with the partial derivative and plugging it into eq.~\eqref{eq:Cabc} will immediately lead to the correct result.
  \end{Sol}

  \paragraph{Step 2:} Using $R^\alpha_{\beta\gamma\delta}w^\beta = (\nabla_\gamma\nabla\delta-\nabla_\delta\nabla_\gamma)w^\alpha$ show
  \begin{equation}
    R^\alpha_{\beta\gamma\delta}w^\beta-R^{(0)\alpha}_{\beta\gamma\delta}w^\beta=2\nabla^{(0)}_{[\gamma}C^\alpha_{\delta]\beta}+2C^\alpha_{\mu[\gamma}C^\mu_{\delta]\beta}\,.
  \end{equation}
  \begin{Sol}
    Let's use the same strategy as before and expression everything in terms of Christoffel symbols.
    I.e. using eqs.~\eqref{eq:covTensor} and~\eqref{eq:covVector} we find
    \begin{equation}
      \begin{aligned}
        \nabla_\gamma\nabla_\delta w^\alpha&=\partial_\gamma\nabla_\delta w^\alpha-{\Gamma^\mu}_{\gamma\delta}\nabla_\mu w^\alpha+{\Gamma^\alpha}_{\gamma\mu}\nabla_\delta w^\mu\\
        &=\partial_\gamma\partial_\delta+\partial_\gamma{\Gamma^\alpha}_{\delta\nu}w^\nu-{\Gamma^\mu}_{\gamma\delta}\partial_\mu w^\alpha-{\Gamma^\mu}_{\gamma\delta}{\Gamma^\alpha}_{\mu\nu}w^\nu\\
        &\quad+{\Gamma^\alpha}_{\gamma\mu}\partial_\delta w^\mu+{\Gamma^\alpha}_{\gamma\mu}{\Gamma^\mu}_{\delta\nu}w^\nu\,,
      \end{aligned}
    \end{equation}
    leading to
    \begin{equation}
      \begin{aligned}
        (\nabla_\gamma\nabla\delta-\nabla_\delta\nabla_\gamma)w^\alpha&=\left(\partial_\gamma{\Gamma^\alpha}_{\delta\nu}+{\Gamma^\alpha}_{\gamma\nu}\partial_\delta+{\Gamma^\alpha}_{\gamma\mu}{\Gamma^\mu}_{\delta\nu}\right.\\
        &\qquad\left.-\partial_\delta{\Gamma^\alpha}_{\gamma\nu}-{\Gamma^\alpha}_{\delta\nu}\partial_\gamma-{\Gamma^\alpha}_{\delta\mu}{\Gamma^\mu}_{\gamma\nu}\right)w^\nu
      \end{aligned}
    \end{equation}
    Now using
    \begin{itemize}
    \item eq.~\eqref{eq:covVector} to replace partial derivatives by the covariant derivative and Christoffel symbols of the background(!) and
    \item the result of step one to replace the Christoffel symbols by Christoffel symbols of the background and $C^\alpha_{\mu\nu}$ tensors,
    \end{itemize}
    it is not hard to arrive at the answer.
  \end{Sol}

  \paragraph{Step 3:} Compute the linearized Einstein equations, i.e. show that
  \begin{equation}
    \begin{aligned}
      \delta G_{\alpha\beta}&=\delta\left(R_{\alpha\beta}-\frac{1}{2}g_{\alpha\beta} R\right)\\
      &=-\frac{1}{2}\Box^{(0)}\bar{h}_{\alpha\beta}^{(1)}-{{{R^{(0)}_\alpha}^\mu}_\beta}^\nu\bar{h}_{\mu\nu}^{(1)}+\nabla_{(\alpha}^{(0)}\nabla^{(0)\mu}\bar{h}^{(1)}_{\beta)\mu}-\frac{1}{2}g_{\alpha\beta}^{(0)}\nabla_\mu^{(0)}\nabla_\nu^{(0)}\bar{h}^{\mu\nu}_{(1)}\\
      &=8\pi T_{\alpha\beta}^{(1)}\,,
    \end{aligned}
  \end{equation}
  where
  \begin{align}
    \Box^{(0)} &\equiv g^{\mu\nu}_{(0)}\nabla_\mu^{(0)}\nabla_\nu^{(0)}\,\\
    \bar{h}_{\alpha\beta}^{(1)} &\equiv h_{\alpha\beta}^{(1)}-\frac{1}{2}g_{\alpha\beta}^{(0)}h^{(1)}\,,\\
    h^{(1)} &\equiv g_{(0)}^{\alpha\beta}h_{\alpha\beta}^{(1)}\,,
  \end{align}
  and we have used $R_{\mu\nu}^{(0)}[g_{\mu\nu}^{(0)}]=0$ for simplicity.
  \begin{Sol}
    Use the results and tricks from the previous step and compute \dots
    See also gr-qc/0501041 section 5 for some more details on this computation.
  \end{Sol}
\end{Exc}
